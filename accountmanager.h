#ifndef ACCOUNTMANAGER_H
#define ACCOUNTMANAGER_H

#include <QObject>
#include <QList>
#include <QUuid>

class Account;

class AccountManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QUuid> accountUids READ accountUids CONSTANT)
public:
    explicit AccountManager(QObject* parent = nullptr);

    QList<QUuid> accountUids() const;
    Q_INVOKABLE Account* loadAccount(const QUuid& uid) const;
    Q_INVOKABLE Account* createAccount() const;

signals:
};

#endif // ACCOUNTMANAGER_H
