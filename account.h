#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QNetworkRequest>
#include <QObject>
#include <QSet>
#include <QString>
#include <QUrl>
#include <QUuid>

#include "todo.h"

class QNetworkAccessManager;
class QTimer;

class Account : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUuid uid READ uid WRITE setUid NOTIFY uidChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QUrl serverUrl READ serverUrl WRITE setServerUrl NOTIFY serverUrlChanged)
    Q_PROPERTY(
            QString privateToken READ privateToken WRITE setPrivateToken NOTIFY privateTokenChanged)
    Q_PROPERTY(bool isReady READ isReady NOTIFY isReadyChanged)
    Q_PROPERTY(bool isRefreshing READ isRefreshing NOTIFY isRefreshingChanged)
    Q_PROPERTY(QString lastError READ lastError NOTIFY lastErrorChanged)
    Q_PROPERTY(QList<Todo*> todos READ todos NOTIFY todosCountChanged)
    Q_PROPERTY(int todosCount READ todosCount NOTIFY todosCountChanged)
    Q_PROPERTY(QSet<qint64> readTodos READ readTodos NOTIFY readTodosChanged)
    Q_PROPERTY(int numberOfUnreadTodos READ numberOfUnreadTodos NOTIFY numberOfUnreadTodosChanged)
public:
    explicit Account(QObject* parent = nullptr);

    QUuid uid() const;
    void setUid(const QUuid& uid);

    QString name() const;
    void setName(const QString& name);

    QUrl serverUrl() const;
    void setServerUrl(const QUrl& serverUrl);

    QString privateToken() const;
    void setPrivateToken(const QString& privateToken);

    bool isReady() const;
    bool isRefreshing() const;

    QString lastError() const;

    int todosCount() const;
    QList<Todo*> todos() const;

    QSet<qint64> readTodos() const;
    Q_INVOKABLE bool isTodoRead(Todo* todo, QSet<qint64> readTodos) const;
    int numberOfUnreadTodos() const;

public slots:

    void save();
    void load();
    void deleteAccount();
    void refresh();
    void toggleTodoIsRead(Todo* todo);
    void markTodoAsDone(Todo* todo);
    void markAllTodosAsDone();
    void markAllTodosAsRead();
    void markAllTodosAsUnread();

signals:

    void uidChanged();
    void nameChanged();
    void serverUrlChanged();
    void privateTokenChanged();
    void isReadyChanged();
    void isRefreshingChanged();
    void lastErrorChanged();
    void todosCountChanged();
    void readTodosChanged();
    void numberOfUnreadTodosChanged();

private:
    QUuid m_uid;
    QString m_name;
    QUrl m_serverUrl;
    QString m_privateToken;

    // Calculated information - to be stored
    QSet<qint64> m_readTodos;

    // Runtime
    QNetworkAccessManager* m_nam;
    bool m_refreshRunning;
    bool m_runRefreshWhenFinished;
    QList<Todo*> m_todos;
    QList<Todo*> m_newTodos;
    QString m_lastError;
    QTimer* m_refreshTimer;

    void setRefreshing(bool refreshing);
    void setLastError(const QString& errorString);
    void clearLastError();
    void setReadTodos(const QVariantList& todoIds);
    QNetworkRequest prepareRequest(const QString& path) const;
    QNetworkAccessManager* getNAM();
    void fetchTodos(const QString& page = QString());
    void parseTodos(const QByteArray& data, const QString& nextPage);
};

#endif // ACCOUNT_H
