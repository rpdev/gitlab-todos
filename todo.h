#ifndef TODO_H
#define TODO_H

#include <QObject>
#include <QVariantMap>

class Todo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap data READ data NOTIFY dataChanged)
    Q_PROPERTY(qint64 id READ id NOTIFY dataChanged)
    Q_PROPERTY(QString title READ title NOTIFY dataChanged)
public:
    explicit Todo(QObject* parent = nullptr);

    QVariantMap data() const;
    void setData(const QVariantMap& data);

    qint64 id() const;
    QString title() const;

signals:

    void dataChanged();

private:
    QVariantMap m_data;

    QString titleForAssigned() const;
    QString titleForMentioned() const;
    QString titleForBuildFailed() const;
    QString titleForMarked() const;
    QString titleForApprovalRequired() const;
    QString titleForUnmergeable() const;
    QString titleForDirectlyAddressed() const;
    QString titleForMergeTrainRemoved() const;
    QString titleForGeneric() const;
};

#endif // TODO_H
