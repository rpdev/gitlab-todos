import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import GitlabTodos 1.0

import "../Controls" as Controls
import "../Fonts" as Fonts

ItemDelegate {
    id: control

    property Account account: Account {}

    clip: true
    text: account.name
    hoverEnabled: true

    contentItem: RowLayout {
        width: control.availableWidth

        Controls.SymbolToolButton {
            symbol: {
                if (!control.account.isReady) {
                    return Fonts.Icons.mdiPersonOff;
                }
                else if (control.account.lastError !== "") {
                    return Fonts.Icons.mdiWarning;
                } else {
                    return Fonts.Icons.mdiPerson;
                }
            }
        }
        Label {
            font: control.font
            text: control.text
            Layout.fillWidth: true
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        ToolButton {
            property int numUnread: account.numberOfUnreadTodos
            text: numUnread
            visible: numUnread > 0
            hoverEnabled: true
            ToolTip.text: qsTr("There are unread todos")
            ToolTip.visible: hovered
        }
    }
    ToolTip.text: {
        if (!control.account.isReady) {
            return qsTr("The private token for the account could not be loaded!")
        } else if (control.account.lastError !== "") {
            return qsTr("There was an error when loading the todos from the server:") + "\n" +
                    control.account.lastError;
        } else {
            return "";
        }
    }
    ToolTip.visible: control.hovered && ToolTip.text !== "";
}
