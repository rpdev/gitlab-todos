import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import GitlabTodos 1.0

import "../Controls" as Controls
import "../Fonts" as Fonts

ItemDelegate {
    id: control

    property Todo todo: Todo {}
    property bool isRead: false

    signal toggleRead()
    signal markDone()

    clip: true
    text: todo.title
    hoverEnabled: true
    highlighted: !isRead

    contentItem: RowLayout {
        id: row
        width: control.availableWidth

        Controls.SymbolToolButton {
            symbol: Fonts.Icons.mdiCheckBox
            onClicked: control.markDone()
            ToolTip.text: qsTr("Mark as Done")
            ToolTip.visible: hovered
        }

        Label {
            text: control.text
            font: control.font
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            Layout.fillWidth: true
            onLinkActivated: Qt.openUrlExternally(link)
        }

        Controls.SymbolToolButton {
            symbol: control.isRead ? Fonts.Icons.mdiMarkAsUnread : Fonts.Icons.mdiMarkEmailRead
            hoverEnabled: true
            onClicked: control.toggleRead()
            ToolTip.text: control.isRead ? qsTr("Mark as Unread") : qsTr("Mark as Read")
            ToolTip.visible: hovered
        }
    }
}
