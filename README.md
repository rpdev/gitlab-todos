# gitlab-todos

A desktop client to keep track of your todos in GitLab.

# Build Instructions

## Fedora

```bash
sudo dnf install -y \
    qt5-qt{base,declarative}-devel \
    qt5-linguist \
    qtkeychain-qt5-devel
mkdir build
cd build
cmake ..
cmake --build .
sudo cmake --build . --target install
```

