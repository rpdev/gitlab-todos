#include "accountmanager.h"
#include "account.h"

#include <QSettings>

/**
 * @brief Manage Accounts.
 */

/**
 * @brief Constructor.
 */
AccountManager::AccountManager(QObject* parent) : QObject(parent) {}

/**
 * @brief Returns the list of account UIDs.
 *
 * This returns a list of UIDs of accounts that are stored.
 */
QList<QUuid> AccountManager::accountUids() const
{
    QList<QUuid> result;
    QSettings settings;
    settings.beginGroup("Accounts");
    const auto keys = settings.childGroups();
    for (const auto& key : keys) {
        auto uid = QUuid::fromString(key);
        if (!uid.isNull()) {
            result << uid;
        }
    }
    settings.endGroup();
    return result;
}

/**
 * @brief Load an account.
 *
 * This loads the account with the given @p uid and returns it.
 *
 * If a null uid is passed in, a nullptr is returned.
 *
 * @note The caller takes ownership of the returned account.
 */
Account* AccountManager::loadAccount(const QUuid& uid) const
{
    Account* result = nullptr;
    if (!uid.isNull()) {
        result = new Account();
        result->setUid(uid);
        result->load();
    }
    return result;
}

/**
 * @brief Returns a new Account object.
 */
Account* AccountManager::createAccount() const
{
    return new Account();
}
