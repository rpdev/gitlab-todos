#include "todo.h"

#include <functional>

/**
 * @class Todo
 * @brief A single todo.
 *
 * This class represents a single todo stored in GitLab.
 */

/**
 * @brief Constructor.
 */
Todo::Todo(QObject* parent) : QObject(parent), m_data() {}

QVariantMap Todo::data() const
{
    return m_data;
}

void Todo::setData(const QVariantMap& data)
{
    if (m_data != data) {
        m_data = data;
        emit dataChanged();
    }
}

/**
 * @brief The ID of the todo.
 */
qint64 Todo::id() const
{
    bool ok;
    auto result = m_data.value("id", -1).toLongLong(&ok);
    if (ok) {
        return result;
    } else {
        return -1;
    }
}

QString Todo::title() const
{
    auto action = m_data.value("action_name").toString();
    QMap<QString, std::function<QString()>> actionMap {
        { "assigned", std::bind(&Todo::titleForGeneric, this) },
        { "mentioned", std::bind(&Todo::titleForGeneric, this) },
        { "build_failed", std::bind(&Todo::titleForGeneric, this) },
        { "marked", std::bind(&Todo::titleForGeneric, this) },
        { "approval_required", std::bind(&Todo::titleForGeneric, this) },
        { "unmergeable", std::bind(&Todo::titleForGeneric, this) },
        { "directly_addressed", std::bind(&Todo::titleForGeneric, this) },
        { "merge_train_removed", std::bind(&Todo::titleForGeneric, this) }
    };
    auto handler = actionMap.value(action, std::bind(&Todo::titleForGeneric, this));
    return handler();
}

QString Todo::titleForAssigned() const
{
    return QString();
}

QString Todo::titleForMentioned() const
{
    return QString();
}

QString Todo::titleForBuildFailed() const
{
    return QString();
}

QString Todo::titleForMarked() const
{
    return QString();
}

QString Todo::titleForApprovalRequired() const
{
    return QString();
}

QString Todo::titleForUnmergeable() const
{
    return QString();
}

QString Todo::titleForDirectlyAddressed() const
{
    return QString();
}

QString Todo::titleForMergeTrainRemoved() const
{
    return QString();
}

QString Todo::titleForGeneric() const
{
    QString template_ = "%1 triggered action %2 on %3: <a href=\"%4\">%5</a>";
    return template_.arg(
            m_data.value("author").toMap().value("name").toString(),
            m_data.value("action_name").toString(), m_data.value("target_type").toString(),
            m_data.value("target_url").toString(),
            m_data.value("target").toMap().value("references").toMap().value("full").toString()
                    + " " + m_data.value("target").toMap().value("title").toString());
}
