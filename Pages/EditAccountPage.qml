import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.0

import GitlabTodos 1.0

import "../Controls" as Controls

Controls.AppPage {
    id: page

    property Account account: Account {}

    signal accountEdited(Account account)

    title: qsTr("Edit Account")
    footer: DialogButtonBox {
        standardButtons: DialogButtonBox.Ok | DialogButtonBox.Cancel
        onRejected: {
            page.StackView.view.pop();
        }
        onAccepted: {
            account.name = nameEdit.text;
            account.serverUrl = urlEdit.text;
            account.privateToken = tokenEdit.text;
            account.save();
            page.accountEdited(page.account);
            page.StackView.view.pop();
        }
    }

    ScrollView {
        id: scrollView

        anchors.fill: parent
        padding: 10

        GridLayout {
            columns: 2
            width: scrollView.availableWidth

            Label {
                text: qsTr("Account Name:")
            }

            Controls.TextInputField {
                id: nameEdit
                Layout.fillWidth: true
                text: account.name
                placeholderText: qsTr("Enter a name for the account")
            }

            Label {
                text: qsTr("Server Address:")
            }

            Controls.TextInputField {
                id: urlEdit
                Layout.fillWidth: true
                text: account.serverUrl
                placeholderText: qsTr("The URL of the GitLab server")
            }

            Label {
                text: qsTr("Private Token:")
            }

            Controls.PasswordField {
                id: tokenEdit
                Layout.fillWidth: true
                text: account.privateToken
                placeholderText: qsTr("Enter a private token to access the GitLab instance")
            }
        }
    }
}
