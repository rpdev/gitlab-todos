import QtQuick 2.14
import QtQuick.Controls 2.14

import "../Controls" as Controls
import "../Data" as Data
import "../Delegates" as Delegates

Controls.AppPage {
    id: page

    title: qsTr("Accounts")


    ScrollView {
        id: scrollView

        anchors.fill: parent

        ListView {
            model: Data.Accounts.accounts
            delegate: Delegates.AccountListDelegate {
                width: parent.width;
                account: modelData
                onClicked: {
                    if (modelData.isReady) {
                        var p = page.StackView.view.push(Qt.resolvedUrl("./TodoListPage.qml"));
                    } else {
                        p = page.StackView.view.push(Qt.resolvedUrl("./EditAccountPage.qml"));
                    }
                    p.account = modelData;
                }
            }
            footer: ItemDelegate {
                text: Data.Accounts.accounts.length > 0 ? qsTr("Connect to another GitLab server...") : qsTr("Connect to a GitLab server...")
                width: parent.width
                onClicked: page.StackView.view.push(Qt.resolvedUrl("./CreateAccountPage.qml"))
            }
        }
    }
}
