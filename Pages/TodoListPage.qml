import QtQuick 2.14
import QtQuick.Controls 2.14

import GitlabTodos 1.0

import "../Configuration" as Configuration
import "../Controls" as Controls
import "../Delegates" as Delegates
import "../Data" as Data

Controls.AppPage {
    id: page

    property Account account: Account {}

    title: account.name + " - " + qsTr("Todos")
    isBusy: account.isRefreshing
    pageActions: [
        Action {
            text: qsTr("Edit")
            onTriggered: {
                let p = page.StackView.view.push(Qt.resolvedUrl("./EditAccountPage.qml"));
                p.account = page.account;
            }
        },
        Action {
            text: qsTr("Refresh")
            onTriggered: page.account.refresh()
            shortcut: Configuration.Shortcuts.refreshShortcut
        },
        Action {
            text: qsTr("Mark All as Read")
            onTriggered: page.account.markAllTodosAsRead()
        },
        Action {
            text: qsTr("Mark All as Unread")
            onTriggered: page.account.markAllTodosAsUnread()
        },
        Action {
            text: qsTr("Mark All as Done")
            onTriggered: page.account.markAllTodosAsDone()
        },
        Action {
            text: qsTr("Delete Account")
            onTriggered: confirmAccountDeletionDialog.open()
        }
    ]

    ScrollView {
        id: scrollView
        anchors.fill: parent

        ListView {
            width: scrollView.availableWidth

            model: page.account.todos
            delegate: Delegates.TodoListDelegate {
                todo: modelData
                width: parent.width
                isRead: page.account.isTodoRead(todo, page.account.readTodos);
                onToggleRead: page.account.toggleTodoIsRead(todo)
                onMarkDone: page.account.markTodoAsDone(todo)
            }
        }
    }

    Dialog {
        id: confirmAccountDeletionDialog

        standardButtons: Dialog.Ok | Dialog.Cancel
        anchors.centerIn: parent
        onAccepted: {
            Data.Accounts.removeAccount(page.account);
            page.StackView.view.pop();
        }

        Label {
            width: Math.min(page.availableWidth / 3 * 2, 500)
            text: qsTr("Do you want to remove the account <strong>%1</strong> from the app? This cannot be undone.").arg(page.account.name)
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
    }
}
