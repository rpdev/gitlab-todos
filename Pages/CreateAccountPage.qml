import QtQuick 2.0

import GitlabTodos 1.0

import "../Data" as Data

EditAccountPage {
    title: qsTr("Connect GitLab Account")
    account: Data.Accounts.createAccount()
    onAccountEdited: Data.Accounts.addAccount(account)
}
