import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.0
import QtQuick.Window 2.14
import Qt.labs.settings 1.0
import Qt.labs.platform 1.1 as Platform

import "Fonts" as Fonts
import "Controls" as Controls
import "Configuration" as Configuration
import "Data" as Data

ApplicationWindow {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("GitLab Todos")

    header: ToolBar {
        id: toolBar

        padding: 10
        width: window.width

        RowLayout {
            width: toolBar.availableWidth
            height: toolBar.availableHeight

            Controls.SymbolToolButton {
                id: backButton
                symbol: Fonts.Icons.mdiArrowBack
                action: backAction
                visible: backAction.enabled
            }

            BusyIndicator {
                visible: stackView.currentItem && stackView.currentItem.isBusy
                height: toolBar.availableHeight
            }

            Label {
                text: stackView.currentItem.title
                Layout.fillWidth: true
            }

            Controls.SymbolToolButton {
                symbol: Fonts.Icons.mdiMenu
                onClicked: pageMenu.open()

                Menu {
                    id: pageMenu

                    modal: true

                    Repeater {
                        model: stackView.pageActions

                        MenuItem {
                            action: modelData
                        }
                    }
                }
            }
        }
    }

    footer: ToolBar {
        id: footerToolbar
        visible: !Data.Notifications.newsRead
        width: window.width
        padding: 10

        RowLayout {
            width: footerToolbar.availableWidth
            height: footerToolbar.availableHeight

            Label {
                text: Data.Notifications.news.title
                Layout.fillWidth: true
            }

            Button {
                text: qsTr("Show")
                onClicked: newsDialog.open()
            }
        }
    }

    QtObject {
        id: d

        property list<Action> standardActions

        standardActions: [
            Action {
                text: qsTr("Updates...")
                onTriggered: newsDialog.open()
            },
            Action {
                text: qsTr("Close")
                shortcut: Configuration.Shortcuts.closeShortcut
                onTriggered: window.close()
            },
            Action {
                text: qsTr("Quit")
                shortcut: Configuration.Shortcuts.quitShortcut
                onTriggered: Qt.quit()
            }
        ]
    }

    Action {
        id: backAction
        text: qsTr("Back")
        shortcut: Configuration.Shortcuts.backShortcut
        onTriggered: stackView.pop()
        enabled: stackView.depth > 1
    }

    Action {
        id: escapeAction
        shortcut: "Esc"
        enabled: backAction.enabled
        onTriggered: backAction.trigger()
    }

    StackView {
        id: stackView

        property list<Action> pageActions

        initialItem: "Pages/AccountsPage.qml"
        anchors.fill: parent
        pageActions: {
            if (currentItem) {
                let result = []
                for (var i = 0; i < currentItem.pageActions.length; ++i) {
                    result.push(currentItem.pageActions[i])
                }
                for (i = 0; i < d.standardActions.length; ++i) {
                    result.push(d.standardActions[i])
                }
                return result
            } else {
                return d.standardActions
            }
        }
        onCurrentItemChanged: pageMenu.close()
    }

    Settings {
        category: "MainWindow"

        property alias width: window.width
        property alias height: window.height
    }

    Platform.SystemTrayIcon {
        id: trayIcon
        visible: true
        icon.source: {
            if (Data.Accounts.totalNumberOfUnreadTodos > 0) {
                return "qrc:/assets/unix/icons/hicolor/64x64/status/net.rpdev.gitlab-todos.unread.png"
            } else {
                return "qrc:/assets/unix/icons/hicolor/64x64/status/net.rpdev.gitlab-todos.all-read.png"
            }
        }
        tooltip: {
            return qsTr("Unread Todos:") + " "
                    + Data.Accounts.totalNumberOfUnreadTodos + "\n" + qsTr(
                        "Open Todos:") + " " + Data.Accounts.totalNumberOfTodos
        }
        menu: Platform.Menu {
            Platform.MenuItem {
                text: qsTr("Show")
                visible: !window.visible
                onTriggered: window.show()
            }
            Platform.MenuItem {
                text: qsTr("Hide")
                visible: window.visible
                onTriggered: window.hide()
            }
            Platform.MenuItem {
                text: qsTr("Quit")
                onTriggered: Qt.quit()
            }
        }
        onActivated: {
            switch (reason) {
            case Platform.SystemTrayIcon.Trigger:
                console.debug("Tray icon: Triggered/clicked")
                window.visible = !window.visible
                if (window.visible) {
                    window.requestActivate()
                }
                return
            }
        }
        onMessageClicked: {
            console.debug("Tray icon: Message clicked")
            window.visible = true
            window.requestActivate()
        }
    }

    Controls.NewsDialog {
        id: newsDialog
    }

    Connections {
        target: Data.Accounts

        function onTotalNumberOfUnreadTodosChanged() {
            if (Data.Accounts.totalNumberOfUnreadTodos > 0 && !window.active) {
                trayIcon.showMessage(
                            qsTr("You have unread todos"),
                            qsTr("You have %1 new unread todos.").arg(
                                Data.Accounts.totalNumberOfUnreadTodos),
                            "qrc:/assets/unix/icons/hicolor/64x64/apps/net.rpdev.gitlab-todos.png",
                            15000)
            }
        }
    }

    Connections {
        target: SingleApplication

        function onInstanceStarted() {
            window.visible = true
            window.requestActivate()
        }
    }

    Connections {
        target: Data.Notifications

        function onNewsAvailable() {
            trayIcon.showMessage(Data.Notifications.news.title,
                                 qsTr("Open the app to read more..."))
        }
    }

    Connections {
        target: Qt.application

        function onStateChanged() {
            switch (Qt.application.state) {
            case Qt.ApplicationActive:
                switch (Qt.platform.os) {
                case "osx":
                {
                    window.visible = true
                    window.requestActivate()
                }
                }
                break
            default:
                break
            }
        }
    }
}
