#include "account.h"

#include <algorithm>

#include <QJsonDocument>
#include <QLoggingCategory>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSettings>
#include <QTimer>
#include <QVariant>
#include <QVariantMap>

#ifdef GITLAB_TODOS_WITH_SYSTEM_QTKEYCHAIN
#include <qt5keychain/keychain.h>
#else
#    include "3rdparty/qtkeychain/keychain.h"
#endif

#include "todo.h"

static Q_LOGGING_CATEGORY(log, "GitlabTodos.Account", QtDebugMsg);

/**
 * @brief Constructor.
 */
Account::Account(QObject* parent)
    : QObject(parent),
      m_uid(QUuid::createUuid()),
      m_name(),
      m_serverUrl(),
      m_privateToken(),
      m_readTodos(),
      m_nam(nullptr),
      m_refreshRunning(false),
      m_runRefreshWhenFinished(false),
      m_todos(),
      m_newTodos(),
      m_refreshTimer(new QTimer(this))
{
    connect(this, &Account::todosCountChanged, this, &Account::numberOfUnreadTodosChanged);
    connect(this, &Account::readTodosChanged, this, &Account::numberOfUnreadTodosChanged);
    m_refreshTimer->setInterval(10 * 60 * 1000); // refresh every 10 minutes
    m_refreshTimer->setSingleShot(false);
    connect(m_refreshTimer, &QTimer::timeout, this, &Account::refresh);
    m_refreshTimer->start();

    // Trigger refresh if there was a request while we fetched changes:
    connect(this, &Account::isRefreshingChanged, this, [=]() {
        if (!isRefreshing() && m_runRefreshWhenFinished) {
            m_runRefreshWhenFinished = false;
            refresh();
        }
    });
}

/**
 * @brief The URL of the GitLab server to connect to.
 */
QUrl Account::serverUrl() const
{
    return m_serverUrl;
}

/**
 * @brief Set the GitLab server URL.
 */
void Account::setServerUrl(const QUrl& serverUrl)
{
    if (m_serverUrl != serverUrl) {
        m_serverUrl = serverUrl;
        emit serverUrlChanged();
        emit isReadyChanged();
    }
}

/**
 * @brief The private token used to authenticate against the GitLab server.
 */
QString Account::privateToken() const
{
    return m_privateToken;
}

/**
 * @brief Set the private token used to authenticate against the GitLab server.
 */
void Account::setPrivateToken(const QString& privateToken)
{
    if (m_privateToken != privateToken) {
        m_privateToken = privateToken;
        emit privateTokenChanged();
        emit isReadyChanged();
        refresh();
    }
}

/**
 * @brief Indicates if the Account is ready.
 *
 * An account is ready if it has both a valid server URL as well as a private token. For example,
 * immediately after loading an account from the settings, it might be in non-ready state, because
 * the secrets are still being loaded from the keychain.
 */
bool Account::isReady() const
{
    return m_serverUrl.isValid() && !m_privateToken.isEmpty();
}

/**
 * @brief Indicated if a refresh is currently running.
 */
bool Account::isRefreshing() const
{
    return m_refreshRunning;
}

/**
 * @brief Saves the account.
 */
void Account::save()
{
    QSettings settings;
    settings.beginGroup("Accounts");
    settings.beginGroup(m_uid.toString());
    settings.setValue("name", m_name);
    settings.setValue("serverUrl", m_serverUrl);
    settings.setValue("readTodos", QVariantList(m_readTodos.constBegin(), m_readTodos.constEnd()));
    settings.endGroup();
    settings.endGroup();
    auto job = new QKeychain::WritePasswordJob("gitlab-todos", this);
    job->setTextData(m_privateToken);
    job->setKey(m_uid.toString());
    job->setAutoDelete(true);
    job->start();
}

/**
 * @brief Loads the account.
 *
 * This restores an account from settings. To use this, make sure you set the uid of the account
 * to one stored:
 *
 * @code
 * auto account = new Account();
 * account->setUid(some_uid);
 * account->load();
 * @endcode
 */
void Account::load()
{
    QSettings settings;
    settings.beginGroup("Accounts");
    settings.beginGroup(m_uid.toString());
    setName(settings.value("name", m_name).toString());
    setServerUrl(settings.value("serverUrl", m_serverUrl).toUrl());
    setReadTodos(settings.value("readTodos",
                                QVariantList(m_readTodos.constBegin(), m_readTodos.constEnd()))
                         .toList());
    settings.endGroup();
    settings.endGroup();
    auto job = new QKeychain::ReadPasswordJob("gitlab-todos", this);
    job->setKey(m_uid.toString());
    job->setAutoDelete(true);
    connect(job, &QKeychain::ReadPasswordJob::finished, this, [=](QKeychain::Job*) {
        // Check if the user meanwhile set the token to something else - e.g. if loading took too
        // long due to the user not allowing read access to the keychain.
        if (m_privateToken.isEmpty()) {
            setPrivateToken(job->textData());
        }
    });
    job->start();
}

/**
 * @brief Deletes the account.
 *
 * This removes the account from the settings and also tries to delete the secrets stored in the
 * keychain.
 */
void Account::deleteAccount()
{
    QSettings settings;
    settings.beginGroup("Accounts");
    settings.remove(m_uid.toString());
    auto job = new QKeychain::DeletePasswordJob("gitlab-todos", this);
    job->setKey(m_uid.toString());
    job->setAutoDelete(true);
    job->start();
}

/**
 * @brief Update the todos by querying the server.
 *
 * This queries the server and checks for new todos.
 */
void Account::refresh()
{
    if (m_refreshRunning && isReady()) {
        m_runRefreshWhenFinished = true;
        return;
    }
    setRefreshing(true);
    fetchTodos();
}

/**
 * @brief Toggle the read state of the @p todo.
 */
void Account::toggleTodoIsRead(Todo* todo)
{
    if (todo != nullptr) {
        auto id = todo->id();
        if (id >= 0) {
            if (m_readTodos.contains(id)) {
                m_readTodos.remove(id);
            } else {
                m_readTodos.insert(id);
            }
            emit readTodosChanged();
            save();
        }
    }
}

/**
 * @brief Mark the @p todo as done.
 *
 * This marks the todo as being done. This is done on the server as well. Internally, this will
 * trigger to requests to the GitLab server: The first marks the todo as done, the second refreshes
 * the list of todos.
 * @param todo
 */
void Account::markTodoAsDone(Todo* todo)
{
    if (todo) {
        auto request = prepareRequest(QString("api/v4/todos/%1/mark_as_done").arg(todo->id()));
        auto reply = getNAM()->post(request, QByteArray());
        if (reply) {
            connect(reply, &QNetworkReply::finished, this, [=]() {
                reply->deleteLater();
                if (reply->error() == QNetworkReply::NoError) {
                    refresh();
                } else {
                    setLastError(tr("Failed to mark todo as done:") + " " + reply->errorString());
                }
            });
        } else {
            setLastError(tr("Failed to mark todo as done: Failed to create network request."));
        }
    }
}

/**
 * @brief Mark all todos as done.
 *
 * This marks all todos as done. Internally, this triggers two requests: The first marks all todos
 * as done, the second refreshes the list of todos.
 */
void Account::markAllTodosAsDone()
{
    auto request = prepareRequest(QString("api/v4/todos/mark_as_done"));
    auto reply = getNAM()->post(request, QByteArray());
    if (reply) {
        connect(reply, &QNetworkReply::finished, this, [=]() {
            reply->deleteLater();
            if (reply->error() == QNetworkReply::NoError) {
                refresh();
            } else {
                setLastError(tr("Failed to mark all todos as done:") + " " + reply->errorString());
            }
        });
    } else {
        setLastError(tr("Failed to mark all todos as done: Failed to create network request."));
    }
}

/**
 * @brief Mark all todos as being read.
 */
void Account::markAllTodosAsRead()
{
    for (const auto& todo : m_todos) {
        m_readTodos.insert(todo->id());
    }
    emit readTodosChanged();
}

/**
 * @brief Mark all todos as being unread.
 */
void Account::markAllTodosAsUnread()
{
    m_readTodos.clear();
    emit readTodosChanged();
}

/**
 * @brief Get the set of IDs of todos that the user marked as read.
 */
QSet<qint64> Account::readTodos() const
{
    return m_readTodos;
}

/**
 * @brief Returns true of the todo has been marked as read.
 */
bool Account::isTodoRead(Todo* todo, QSet<qint64> readTodos) const
{
    Q_UNUSED(readTodos);
    if (todo) {
        return m_readTodos.contains(todo->id());
    }
    return false;
}

/**
 * @brief Return the number of unread todos.
 */
int Account::numberOfUnreadTodos() const
{
    return std::count_if(m_todos.constBegin(), m_todos.constEnd(),
                         [=](const Todo* todo) { return !m_readTodos.contains(todo->id()); });
}

/**
 * @brief Returns the last error string.
 */
QString Account::lastError() const
{
    return m_lastError;
}

/**
 * @brief Return the number of todos the account currently holds.
 */
int Account::todosCount() const
{
    return m_todos.length();
}

/**
 * @brief Return the list of todos.
 */
QList<Todo*> Account::todos() const
{
    return m_todos;
}

/**
 * @brief A unique ID identifying the account internally.
 */
QUuid Account::uid() const
{
    return m_uid;
}

/**
 * @brief Set the account's unique ID.
 */
void Account::setUid(const QUuid& uid)
{
    if (m_uid != uid) {
        m_uid = uid;
        emit uidChanged();
    }
}

/**
 * @brief A human readable name representing the account.
 */
QString Account::name() const
{
    return m_name;
}

/**
 * @brief Set the account name.
 */
void Account::setName(const QString& name)
{
    if (m_name != name) {
        m_name = name;
        emit nameChanged();
    }
}

void Account::setRefreshing(bool refreshing)
{
    if (!refreshing) {
        qDeleteAll(m_newTodos);
        m_newTodos.clear();
    }
    m_refreshRunning = refreshing;
    emit isRefreshingChanged();
}

void Account::setLastError(const QString& errorString)
{
    if (m_lastError != errorString) {
        m_lastError = errorString;
        emit lastErrorChanged();
    }
}

/**
 * @brief Clears the last error state.
 */
void Account::clearLastError()
{
    if (!m_lastError.isEmpty()) {
        m_lastError.clear();
        emit lastErrorChanged();
    }
}

void Account::setReadTodos(const QVariantList& todoIds)
{
    m_readTodos.clear();
    for (const auto& todoId : todoIds) {
        bool ok;
        auto id = todoId.toLongLong(&ok);
        if (ok) {
            m_readTodos.insert(id);
        }
    }
}

QNetworkRequest Account::prepareRequest(const QString& path) const
{
    QUrl url(m_serverUrl);
    if (!url.toString().endsWith("/")) {
        url.setPath(url.path() + "/");
    }
    url.setPath(url.path() + path);
    QNetworkRequest request;
    request.setUrl(url);
    request.setRawHeader("PRIVATE-TOKEN", m_privateToken.toUtf8());
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    return request;
}

QNetworkAccessManager* Account::getNAM()
{
    clearLastError();
    if (!m_nam) {
        m_nam = new QNetworkAccessManager(this);
        m_nam->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    }
    return m_nam;
}

void Account::fetchTodos(const QString& page)
{
    auto request = prepareRequest("api/v4/todos");
    if (!page.isEmpty()) {
        auto url = request.url();
        url.setQuery("page=" + page);
        request.setUrl(url);
    }
    auto reply = getNAM()->get(request);
    if (!reply) {
        qCWarning(log) << "Failed to create network request!";
        setRefreshing(false);
        return;
    }
    connect(reply, &QNetworkReply::finished, this, [=]() {
        reply->deleteLater();
        if (reply->error() == QNetworkReply::NoError) {
            parseTodos(reply->readAll(), reply->rawHeader("x-next-page"));
        } else {
            setLastError(tr("Failed to fetch todos: %1").arg(reply->errorString()));
            setRefreshing(false);
        }
    });
}

void Account::parseTodos(const QByteArray& data, const QString& nextPage)
{
    QJsonParseError error;
    auto doc = QJsonDocument::fromJson(data, &error);
    if (error.error != QJsonParseError::NoError) {
        setLastError(tr("Failed to parse response from server: %1").arg(error.errorString()));
        setRefreshing(false);
        return;
    }
    auto todos = doc.toVariant().toList();
    for (const auto& entry : qAsConst(todos)) {
        auto todo = new Todo(this);
        todo->setData(entry.toMap());
        m_newTodos.append(todo);
    }
    if (!nextPage.isEmpty()) {
        fetchTodos(nextPage);
        return;
    }
    qDeleteAll(m_todos);
    m_todos = m_newTodos;
    m_newTodos.clear();
    setRefreshing(false);
    emit todosCountChanged();
}
