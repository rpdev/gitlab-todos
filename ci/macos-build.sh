#!/bin/bash

set -e

BUILD_DIR=$PWD/build-macos

rm -rf $BUILD_DIR

if [ ! -d "$QT_INSTALLATION_DIR" ]; then
    echo "The variable QT_INSTALLATION_DIR is not set"
    exit 1
fi

if [ -z "$QT_VERSION" ]; then
  echo "The variable QT_VERSION is not set"
  exit 1
fi

if [ -z "$OSX_DEPLOYMENT_TARGET" ]; then
    OSX_DEPLOYMENT_TARGET=10.13
fi

if [ -z "$MACOS_TEAM_ID" ]; then
    # Default Team ID to use:
    MACOS_TEAM_ID="786Z636JV9"
fi

QT_DIR=$QT_INSTALLATION_DIR/$QT_VERSION/clang_64

#rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR
cd $BUILD_DIR

cmake \
    -GNinja \
    -DCMAKE_OSX_DEPLOYMENT_TARGET=$OSX_DEPLOYMENT_TARGET \
    -DCMAKE_PREFIX_PATH=$QT_DIR \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
    -DCMAKE_C_COMPILER_LAUNCHER=ccache \
    -DCMAKE_OSX_ARCHITECTURES=x86_64 \
    -DGITLAB_TODOS_WITH_SYSTEM_QTKEYCHAIN=OFF \
    $CMAKE_EXTRA_FLAGS \
    ..
cmake --build .

# Include Qt Runtime in App Bundle. Also sign the bundle
# and prepare it for notarization:
$QT_DIR/bin/macdeployqt \
    gitlab-todos.app/ \
    -qmldir=.. \
    -appstore-compliant \
    -sign-for-notarization="Developer ID Application: Martin Hoeher (786Z636JV9)"

# Create a zip archive suitable for uploading to the notarization
# service:
ditto \
    -ck --rsrc \
    --sequesterRsrc \
    --keepParent \
    "gitlab-todos.app" "gitlab-todos.zip"

# Make sure the app has been signed:
codesign -v gitlab-todos.app

# Upload the archive for notarization:
xcrun notarytool submit \
    "gitlab-todos.zip" \
    --wait \
    --apple-id "martin@rpdev.net" \
    --team-id "$MACOS_TEAM_ID" \
    --password "$GITLAB_TODOS_STORE_KEY"

# Include the notarization ticket in the app bundle:
xcrun stapler staple "gitlab-todos.app"

# Prepare a "beautified" folder:
mkdir dmg.in
cp -R gitlab-todos.app dmg.in
cp ../assets/macos/DS_Store ./dmg.in/.DS_Store
cd dmg.in
ln -s /Applications ./Applications
cd ../

# Create DMG file:
i="0"
while [ $i -lt 5 ]; do
    if ! hdiutil create \
        -volname gitlab-todos \
        -srcfolder ./dmg.in \
        -ov -format UDZO \
        gitlab-todos.dmg; then
        echo "Creating disk image failed - retrying in 30s"
        sleep 30
    else
        break
    fi
    i=$[$i+1]
done
