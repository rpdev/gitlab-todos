#!/bin/bash

set -e

if [ -n "$CI" ]; then
    if [ ! -f /bin/find ]; then
        dnf install -y --nogpgcheck findutils ccache
    fi
fi

if git describe --tags 2>&1 > /dev/null; then
    export VERSION=$(git describe --tags)
else
    export VERSION=0.0.0
fi
TARGET=win64

BUILD_DIR=build-win64
DEPLOY_DIR=deploy-win64
MXE_DIR=x86_64-w64-mingw32
CMAKE=mingw64-cmake
QMAKE=mingw64-qmake-qt5
INSTALLER_FILE=win64-installer.nsis
INSTALLER_OUTPUT_FILE=gitlab-todos-Windows-64bit.exe
INSTALLER_OUTPUT_TARGET_FILE=gitlab-todos-${VERSION}-Windows-64bit.exe
EXTRA_LIBS="\
    /usr/$MXE_DIR/sys-root/mingw/bin/libcrypto-3-x64.dll \
    /usr/$MXE_DIR/sys-root/mingw/bin/libssl-3-x64.dll"

mkdir -p $BUILD_DIR
cd $BUILD_DIR

$CMAKE \
    -GNinja \
    -DCMAKE_BUILD_TYPE=Release \
    -DUSE_CREDENTIAL_STORE=ON \
    -DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
    -DCMAKE_C_COMPILER_LAUNCHER=ccache \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DGITLAB_TODOS_WITH_SYSTEM_QTKEYCHAIN=OFF \
    -DQTKEYCHAIN_STATIC=ON \
    ..
cmake --build .
rm -rf $PWD/_
DESTDIR=$PWD/_ cmake --build . --target install
mkdir -p "$PWD/../$DEPLOY_DIR"
cp -r _/usr/bin _/usr/share "$PWD/../$DEPLOY_DIR"

# Remove some extra files we don't need to deploy
rm -f \
    $PWD/../$DEPLOY_DIR/bin/*.dll.a \
    $PWD/../$DEPLOY_DIR/bin/*.prl

cd ..

cp \
    $EXTRA_LIBS \
    /usr/$MXE_DIR/sys-root/mingw/bin/libEGL.dll \
    /usr/$MXE_DIR/sys-root/mingw/bin/libGLESv2.dll \
    \
    $DEPLOY_DIR/bin/

if [ ! -f xwindeployqt ]; then
    wget https://gitlab.com/rpdev/xwindeployqt/raw/master/xwindeployqt
    chmod +x xwindeployqt
fi
./xwindeployqt \
    --driver fedora-mingw \
    --qml-dir . \
    --plugins platforms \
    --plugins imageformats \
    --plugins sqldrivers \
    --plugins bearer \
    --mingw-arch $MXE_DIR \
    $DEPLOY_DIR/bin/

# Strip debug symbols to reduce file size:
find $DEPLOY_DIR/bin/ \
    -name '*.exe' -or -name '*.dll' \
    -exec mingw-strip {} \;

cp assets/windows/nsis/$INSTALLER_FILE $DEPLOY_DIR/
cd $DEPLOY_DIR
makensis $INSTALLER_FILE

mv $INSTALLER_OUTPUT_FILE $INSTALLER_OUTPUT_TARGET_FILE
