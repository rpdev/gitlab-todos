#!/bin/bash

set -e

PREFIX_PATH=$QT_ROOT

desktop-file-validate assets/unix/net.rpdev.GitlabTodos.desktop

export QT_QPA_PLATFORM=minimal

mkdir -p build-appimage
cd build-appimage

cmake \
    -GNinja \
    -DCMAKE_PREFIX_PATH=$QT_ROOT \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
    -DCMAKE_C_COMPILER_LAUNCHER=ccache \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DGITLAB_TODOS_WITH_SYSTEM_QTKEYCHAIN=OFF \
    -DQTKEYCHAIN_STATIC=ON \
    ..
cmake --build .
rm -rf AppImageBuild
DESTDIR=$PWD/AppImageBuild cmake --build . --target install

# Remove unnecessary stuff:
rm -rf AppImageBuild/usr/{include,lib,mkspecs}

# Add symlinks to app root, so they are found by linuxdeployqt:
ln -s "./usr/share/applications/net.rpdev.GitlabTodos.desktop" AppImageBuild/default.desktop
ln -s "./usr/share/icons/hicolor/256x256/apps/net.rpdev.gitlab-todos.png" \
    AppImageBuild/net.rpdev.GitlabTodos.png

if git describe --tags 2>&1 > /dev/null; then
    export VERSION=$(git describe --tags)
else
    export VERSION=0.0.0
fi

LINUXDEPLOYQT="./linuxdeployqt-x86_64.AppImage"
if [ ! -f "$LINUXDEPLOYQT" ]; then
    curl -L -o "$LINUXDEPLOYQT" \
        https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage
    chmod +x "$LINUXDEPLOYQT"
fi

"$LINUXDEPLOYQT" --appimage-extract

./squashfs-root/AppRun AppImageBuild/usr/bin/gitlab-todos \
    -qmldir=.. -qmake=$QT_ROOT/bin/qmake \
    -appimage \
    -extra-plugins=platforminputcontexts
