#!/bin/bash

set -e

TOOLS_DIR="$(cd "$(dirname "$0")" && echo $PWD)"
cd $TOOLS_DIR
cd ..

cd assets/svg

for file in $(find . -name \*.svg); do
    for size in 8 16 20 22 24 32 36 40 44 46 48 64 72 96 128 150 192 256 310 480 512 1024; do
        target_file=$TOOLS_DIR/../assets/unix/icons/hicolor/${size}x${size}/${file}
        target_file="$(dirname "$target_file")/$(basename "$target_file" .svg).png"
        target_dir="$(dirname "$target_file")"
        mkdir -p "$target_dir"
        inkscape --export-filename="$target_file" -w $size -h $size -C $file
    done
done
