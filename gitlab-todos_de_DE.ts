<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>Account</name>
    <message>
        <location filename="account.cpp" line="246"/>
        <source>Failed to mark todo as done:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="account.cpp" line="250"/>
        <source>Failed to mark todo as done: Failed to create network request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="account.cpp" line="271"/>
        <source>Failed to mark all todos as done:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="account.cpp" line="275"/>
        <source>Failed to mark all todos as done: Failed to create network request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="account.cpp" line="474"/>
        <source>Failed to fetch todos: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="account.cpp" line="485"/>
        <source>Failed to parse response from server: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountListDelegate</name>
    <message>
        <location filename="Delegates/AccountListDelegate.qml" line="46"/>
        <source>There are unread todos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Delegates/AccountListDelegate.qml" line="52"/>
        <source>The private token for the account could not be loaded!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Delegates/AccountListDelegate.qml" line="54"/>
        <source>There was an error when loading the todos from the server:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="Pages/AccountsPage.qml" line="11"/>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/AccountsPage.qml" line="34"/>
        <source>Connect to another GitLab server...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/AccountsPage.qml" line="34"/>
        <source>Connect to a GitLab server...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateAccountPage</name>
    <message>
        <location filename="Pages/CreateAccountPage.qml" line="8"/>
        <source>Connect GitLab Account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditAccountPage</name>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="16"/>
        <source>Edit Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="43"/>
        <source>Account Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="50"/>
        <source>Enter a name for the account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="54"/>
        <source>Server Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="61"/>
        <source>The URL of the GitLab server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="65"/>
        <source>Private Token:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/EditAccountPage.qml" line="72"/>
        <source>Enter a private token to access the GitLab instance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewsDialog</name>
    <message>
        <location filename="Controls/NewsDialog.qml" line="12"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Controls/NewsDialog.qml" line="20"/>
        <source>Remind me later</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeychain::DeletePasswordJobPrivate</name>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="165"/>
        <source>Could not open keystore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="171"/>
        <source>Could not remove private key from keystore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_haiku.cpp" line="177"/>
        <source>Password not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="548"/>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="556"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="574"/>
        <source>Could not open wallet: %1; %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="104"/>
        <source>Password entry not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="108"/>
        <source>Could not decrypt data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeychain::JobPrivate</name>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="265"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="509"/>
        <source>Access to keychain denied</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeychain::PlainTextStore</name>
    <message>
        <location filename="3rdparty/qtkeychain/plaintextstore.cpp" line="65"/>
        <source>Could not store data in settings: access error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/plaintextstore.cpp" line="67"/>
        <source>Could not store data in settings: format error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/plaintextstore.cpp" line="85"/>
        <source>Could not delete data from settings: access error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/plaintextstore.cpp" line="87"/>
        <source>Could not delete data from settings: format error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/plaintextstore.cpp" line="104"/>
        <source>Entry not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeychain::ReadPasswordJobPrivate</name>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="50"/>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="363"/>
        <source>Entry not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="58"/>
        <source>Could not open keystore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="66"/>
        <source>Could not retrieve private key from keystore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="73"/>
        <source>Could not create decryption cipher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_haiku.cpp" line="96"/>
        <source>Password not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="178"/>
        <source>D-Bus is not running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="187"/>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="197"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="286"/>
        <source>No keychain service available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="288"/>
        <source>Could not open wallet: %1; %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="333"/>
        <source>Access to keychain denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="354"/>
        <source>Could not determine data type: %1; %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="372"/>
        <source>Unsupported entry type &apos;Map&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="375"/>
        <source>Unknown kwallet entry type &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="32"/>
        <source>Password entry not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="36"/>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="139"/>
        <source>Could not decrypt data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QKeychain::WritePasswordJobPrivate</name>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="93"/>
        <source>Could not open keystore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="116"/>
        <source>Could not create private key generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="123"/>
        <source>Could not generate new private key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="131"/>
        <source>Could not retrieve private key from keystore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="139"/>
        <source>Could not create encryption cipher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_android.cpp" line="147"/>
        <source>Could not encrypt data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_haiku.cpp" line="144"/>
        <source>Password not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="415"/>
        <source>D-Bus is not running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="425"/>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="452"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="468"/>
        <source>Could not open wallet: %1; %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="78"/>
        <source>Credential size exceeds maximum size of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="87"/>
        <source>Credential key exceeds maximum size of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="92"/>
        <source>Writing credentials failed: Win32 error code %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_win.cpp" line="162"/>
        <source>Encryption failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_haiku.cpp" line="72"/>
        <source>error 0x%1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="225"/>
        <source>Access to keychain denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="227"/>
        <source>No keyring daemon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="229"/>
        <source>Already unlocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="231"/>
        <source>No such keyring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="233"/>
        <source>Bad arguments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="235"/>
        <source>I/O error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="237"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="239"/>
        <source>Keyring already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="241"/>
        <source>No match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/keychain_unix.cpp" line="246"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="3rdparty/qtkeychain/libsecret.cpp" line="119"/>
        <source>Entry not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodoListDelegate</name>
    <message>
        <location filename="Delegates/TodoListDelegate.qml" line="31"/>
        <source>Mark as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Delegates/TodoListDelegate.qml" line="47"/>
        <source>Mark as Unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Delegates/TodoListDelegate.qml" line="47"/>
        <source>Mark as Read</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="Pages/TodoListPage.qml" line="16"/>
        <source>Todos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="20"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="27"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="32"/>
        <source>Mark All as Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="36"/>
        <source>Mark All as Unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="40"/>
        <source>Mark All as Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="44"/>
        <source>Delete Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="Pages/TodoListPage.qml" line="79"/>
        <source>Do you want to remove the account &lt;strong&gt;%1&lt;/strong&gt; from the app? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="18"/>
        <source>GitLab Todos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="97"/>
        <source>Updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="101"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="106"/>
        <location filename="main.qml" line="186"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="115"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="170"/>
        <source>Unread Todos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="171"/>
        <source>Open Todos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="84"/>
        <location filename="main.qml" line="176"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="181"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="216"/>
        <source>You have unread todos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="217"/>
        <source>You have %1 new unread todos.</source>
        <oldsource>You have new unread todos.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.qml" line="239"/>
        <source>Open the app to read more...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
