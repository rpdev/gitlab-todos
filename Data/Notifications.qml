pragma Singleton

import QtQuick 2.0
import Qt.labs.settings 1.0

Item {
    id: notifications

    signal newsAvailable()

    readonly property alias news: settings.latestNews
    property alias newsRead: settings.latestNewsRead

    Timer {
        interval: 1000 * 60 * 60 * 24 // Check once a day
        repeat: true
        running: true
        onTriggered: d.fetchNotifications()
    }

    QtObject {
        id: d

        Component.onCompleted: fetchNotifications()

        function fetchNotifications() {
            var doc = new XMLHttpRequest();
            doc.onreadystatechange = function() {
                if (doc.readyState === XMLHttpRequest.DONE) {
                    var a = JSON.parse(doc.response);
                    if (typeof(a.timestamp) === "string" && typeof(a.title) === "string" && typeof(a.text) === "string") {
                        let doUpdate = false;
                        try {
                            if (a.timestamp !== settings.latestNews.timestamp) {
                                doUpdate = true;
                            }
                        } catch (e) {
                            doUpdate = true;
                        }

                        if (doUpdate) {
                            settings.latestNews = a;
                            settings.latestNewsRead = false;
                        }
                        if (doUpdate || !settings.latestNewsRead) {
                            notifications.newsAvailable();
                        }
                    }
                }
            }

            doc.open("GET", "https://gitlab.com/rpdev/gitlab-todos/-/raw/main/notifications/news.json");
            doc.send();
        }
    }

    Settings {
        id: settings

        category: "Notifications"

        property var latestNews: {
            "timestamp": "Tue Sep 21 22:20:41 CEST 2021",
            "title": "Update Notifications",
            "text": "Welcome to gitlab-todos! You will get notified about updates from time to time.\n\nMeanwhile...\n\n- If you have any issues, please file a bug report in our [issue tracker](https://gitlab.com/rpdev/gitlab-todos/-/issues).\n- Feel free to follow us on [Twitter](https://twitter.com/rpdevapps) to get informed about the latest updates.\n\nThanks!"
        }

        property bool latestNewsRead: true
    }
}
