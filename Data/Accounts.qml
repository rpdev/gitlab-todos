pragma Singleton

import QtQuick 2.0

import GitlabTodos 1.0

Item {
    id: accounts

    property var accounts: []
    readonly property int totalNumberOfUnreadTodos: {
        let result = 0;
        for (let i = 0; i < accounts.accounts.length; ++i) {
            result += accounts.accounts[i].numberOfUnreadTodos;
        }
        return result;
    }
    readonly property int totalNumberOfTodos: {
        let result = 0;
        for (let i = 0; i < accounts.accounts.length; ++i) {
            result += accounts.accounts[i].todos.length;
        }
        return result;
    }

    function addAccount(account) {
        if (account) {
            let newAccounts = [];
            let oldAccounts = accounts.accounts;
            for (let i = 0; i < oldAccounts.length; ++i) {
                newAccounts.push(oldAccounts[i]);
            }
            newAccounts.push(account);
            accounts.accounts = newAccounts;
        }
    }

    function removeAccount(account) {
        if (account) {
            let newAccounts = [];
            let oldAccounts = accounts.accounts;
            for (let i = 0; i < oldAccounts.length; ++i) {
                let a = oldAccounts[i];
                if (a !== account) {
                    newAccounts.push(a);
                }
            }
            account.deleteAccount();
            accounts.accounts = newAccounts;
        }
    }

    function createAccount() {
        return accountManager.createAccount();
    }

    AccountManager {
        id: accountManager
    }

    Component.onCompleted: {
        let list = [];
        let uids = accountManager.accountUids;
        for (let i = 0; i < uids.length; ++i) {
            let uid = uids[i];
            list.push(accountManager.loadAccount(uid));
        }
        accounts.accounts = list;
    }
}
