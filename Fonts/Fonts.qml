pragma Singleton

import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    id: fonts

    readonly property string icons: materialDesignIcons.name
    readonly property alias defaultFont: dummy.font

    FontLoader {
        id: materialDesignIcons
        source: "./material-design-icons/font/MaterialIconsOutlined-Regular.otf"
    }

    Label {
        id: dummy
    }
}
