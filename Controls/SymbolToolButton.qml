import QtQuick 2.10
import QtQuick.Controls 2.3 as QQC2
import QtQuick.Controls.Material 2.12

import "." as C
import "../Fonts" as Fonts

QQC2.ToolButton {
    id: button

    property alias symbol: button.text

    font.family: Fonts.Fonts.icons
    font.pointSize: Fonts.Fonts.defaultFont.pointSize * 1.6
}
