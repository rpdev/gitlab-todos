import QtQuick 2.0
import QtQuick.Controls 2.5

import "../Data" as Data

Dialog {
    id: newsDialog

    title: Data.Notifications.news.title
    footer: DialogButtonBox {
        Button {
            text: qsTr("OK")
            onClicked: {
                newsDialog.close();
                Data.Notifications.newsRead = true;
            }
        }

        Button {
            text: qsTr("Remind me later")
            visible: !Data.Notifications.newsRead
            onClicked: {
                newsDialog.close();
            }
        }
    }

    anchors.centerIn: parent
    modal: true

    ScrollView {
        id: newsScrollView

        width: window.contentItem.width * 0.8
        height: Math.min(window.contentItem.height * 0.8)

        Label {
            id: newsTextLabel

            text: Data.Notifications.news.text
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            textFormat: Text.MarkdownText
            width: newsScrollView.availableWidth
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }
}
