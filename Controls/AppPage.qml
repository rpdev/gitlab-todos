import QtQuick 2.14
import QtQuick.Controls 2.14

Page {
    id: page

    property list<Action> pageActions
    property bool isBusy: false
}
