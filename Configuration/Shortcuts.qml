pragma Singleton

import QtQuick 2.14

Item {
    id: shortcuts

    readonly property int defaultQuitShortcut: StandardKey.Quit
    readonly property int defaultCloseShortcut: StandardKey.Close
    readonly property int defaultRefreshShortcut: StandardKey.Refresh
    readonly property int defaultBackShortcut: StandardKey.Back

    readonly property var quitShortcut: {
        return defaultQuitShortcut;
    }

    readonly property var closeShortcut: {
        return defaultCloseShortcut;
    }

    readonly property var refreshShortcut: {
        return defaultRefreshShortcut;
    }

    readonly property var backShortcut: {
        return defaultBackShortcut;
    }
}
