#include <QApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <SingleApplication>

#include "account.h"
#include "accountmanager.h"
#include "todo.h"

int main(int argc, char* argv[])
{
    QCoreApplication::setApplicationName("gitlab-todos");
    QCoreApplication::setOrganizationDomain("rpdev.net");
    QCoreApplication::setOrganizationName("RPdev");
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    SingleApplication app(argc, argv);
    QApplication::setWindowIcon(
            QIcon(":/assets/unix/icons/hicolor/64x64/apps/net.rpdev.gitlab-todos.png"));
    QApplication::setDesktopFileName("net.rpdev.GitlabTodos");
    app.setQuitOnLastWindowClosed(false);

    qmlRegisterType<Account>("GitlabTodos", 1, 0, "Account");
    qmlRegisterType<AccountManager>("GitlabTodos", 1, 0, "AccountManager");
    qmlRegisterType<Todo>("GitlabTodos", 1, 0, "Todo");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("SingleApplication", &app);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
            &engine, &QQmlApplicationEngine::objectCreated, &app,
            [url](QObject* obj, const QUrl& objUrl) {
                if (!obj && url == objUrl)
                    QCoreApplication::exit(-1);
            },
            Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
